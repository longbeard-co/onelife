var $ = jQuery.noConflict();
/*********************
ANONYMOUS FUNCTIONS
*********************/
// if ( !$('body').hasClass('logged-in') ) {
$(document).ready(function () {
    // I'll take all the help I can get 
    console.log("✝JMJ");
    lb_pagination('#div_block-3-873');
    lb_pagination('#div_block-9-873');
    lb_pagination('.news-highlights-section');
    showMore('#div_block-9-873');
    highlightsSlider();
    gearSlider();
    partnersSlider();
    mobileButton();
    wpmlLimit();
    cardClick();
    registerComplete();
    // buttonClass();
    mobileLangSwitch();
    testimonials();
    history();
    popup();
    if($('.testimonies-wrapper')) {
        extraCardSlider('.testimonies-wrapper');
    }

    if($('#new_columns-31-16')) {
        extraCardSlider('#new_columns-31-16');
    }

    openSearch('#fancy_icon-36-14');
    accordion('accordion');
    eventDetailsSlider('.speaker:not(.not-enough) .speakers');
    eventDetailsSlider('.performer:not(.not-enough) .performers');
    eventDetailNav('.ct-div-block.nav');
   // readMore('.testimonies-wrapper .card h4 p');

});
//  };

// window.sr = ScrollReveal();
// sr.reveal('.lb-sr', {
//     scale: 1,
//     origin: 'bottom',
//     mobile: false,
//     distance: '10px',
//     delay: 350,
//     duration: 650
// });

/*********************
DECLARED FUNCTIONS
*********************/

// simple tabs
function lb_pagination(element) {
    var lbButton = $(element + ' .pagination-nav a');

    $(lbButton).first().addClass("active");

    $(element + ' .pagination-body').each(function () {
        $(this).removeClass('active');
    });

    $(element + ' .pagination-body').first().addClass("active");

    // click function
    $(lbButton).click(function (e) {
        
        e.preventDefault();
        var lbHref = $(this).attr('href').substr(1);
        // console.log("yes");

        // remove active class
        $(lbButton).each(function () {
            $(this).removeClass('active');
        });
        // add to link clicked
        $(this).addClass('active');

        // remove active class
        $(element + ' .pagination-body').each(function () {
            $(this).removeClass('active');
        });
        // add to body relative to link clicked
        $(element + ' .pagination-body#' + lbHref).addClass('active');
    });
}

function showMore(element) {

    $(element + ' .ct-link-button').click(function() {
        event.preventDefault();

        if ($(window).width() < 601) {
            $(element + ' .alm-video-wrapper:nth-child(n+2)').addClass('show-more');
        } else {
            $(element + ' .alm-video-wrapper:nth-child(n+9)').addClass('show-more');
        }
        $(this).fadeOut();
    });


}


function gearSlider() {
    $('.gear-wrapper').slick({
            dots: false,
            infinite: true,
            speed: 300,
            autoplay: true,
            pauseOnHover: true,
            arrows: true,
            slidesToShow: 3,
            responsive: [
            {
              breakpoint: 9999,
              settings: "unslick",
            },
            {
              breakpoint: 600,
              settings: {
                nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(-1254 -507)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
               prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(1284 537) rotate(180)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                
              }
            }
          ]
        });
}
function buttonClass() {
    $('li.menu-item-495').addClass('button lb-sr');
}

function mobileLangSwitch() {
    $('.mobile-menu-wrapper ul').append('<li class="menu-item menu-item-type-post_type menu-item-object-page">')
}

function testimonials() {
     // var cookieUser = '<?php echo $_COOKIE['test'];?>';
     if($(window).width() >= 1025) {
    $('.testimonies-wrapper .card').live('click', function() { 
        console.log('test');
        var featured = $('.testimony-featured > .card');
        // var card = $(this)
        $(this).clone().appendTo(".testimony-featured");
        $('.testimony-featured > .first').appendTo('.testimonies-wrapper');

        $(this).remove();
        $('.testimonies-wrapper .card').each(function(){
            $(this).removeClass('first');
        });
        $(".testimony-featured .card").addClass('first');

        $('html,body').animate({
            scrollTop: $('#code_block-10-22').offset().top
        });
        
    });
}
}


function readMore(element) {
    $(element).each(function() {
        // Amount of words to show
        wordLimit = 50;
        // Get the para and make array
        arrayPara = $(this).text().split(' ');

        // split array into one to show and one to hide
        arrayShow = arrayPara.slice(0, wordLimit);
        arrayHide = arrayPara.slice(wordLimit);

        // replace text
        $(this).text(arrayShow.join(' ') + '...');    
    });
}

function eventDetailNav(element) {
    $(element).find('a').click(function(){
        $(element).find('a').each(function() {
            $(this).removeClass('active');
        });
        $('.pageable .ct-code-block').each(function() {
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        var href = $(this).attr('href');
        var classNeeded = href.slice(1);

        $('.pageable .ct-code-block.'+classNeeded).addClass("active");
    });
}

function accordion(elementClassName) {
    var acc = document.getElementsByClassName(elementClassName);
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }
}

function openSearch(element) {
    $(element).click(function(){
        $(this).parent().toggleClass('active');
        $('.social').toggleClass('active');
    });
}

function wpmlLimit() {
    $('.translator a > span').text(function(index, currentText) {
        return currentText.substr(0, 3);
    });
}

function cardClick() {
    $('.our-history-item .close-button').click(function(){
        $('.our-history-item').removeClass('active');
    });
}

function registerComplete() {

    $('.gf_step_completed > span.gf_step_number').html('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.532 17.711"><defs><style>.a{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px;}</style></defs><path class="a" d="M8479.6,1005.638l5.985,6.9,10.548-15.711" transform="translate(-8478.597 -995.824)"/></svg>');
}

function extraCardSlider(elementWrapper) {
    'use strict';

    var settings = {
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(-1254 -507)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
        prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(1284 537) rotate(180)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
        autoplay: true,
        responsive: [{
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
            }
        }, ]
    }


    var windowW = $(window);

    if ($(windowW).width() < 1025) {
        $(elementWrapper).slick(settings);
    }
 
    $(windowW).on('resize', function() {
        if ($(windowW).width() > 1024) {
            $(elementWrapper).slick('unslick');
        }

        if ($(windowW).width() < 1025) {
            $(elementWrapper).slick(settings); 
        }

    });
}

function highlightsSlider() {
    if ($('body').hasClass('home') || $('body').hasClass('page-id-18') || $('body').hasClass('page-id-371') || $('body').hasClass('page-id-4365') || $('body').hasClass('page-id-4375') || $('body').hasClass('page-id-5594')) {
        $('.highlights-wrapper').slick({
            lazyLoad: 'ondemand',
            dots: false,
            infinite: true,
            speed: 300,
            autoplay: true,
            pauseOnHover: true,
            arrows: true,
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(-1254 -507)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(1284 537) rotate(180)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
            slidesToShow: 3,
            responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
              }
            }
          ]
        });
    }
}

function eventDetailsSlider(elementClass) {
    if ($('body').hasClass('page-id-29') || $('body').hasClass('page-id-364') || $('body').hasClass('page-id-8249') || $('body').hasClass('page-id-8250') ) {
        $(elementClass).slick({
            centerMode: true,
            slidesToShow: 3,
            centerPadding: '2rem',
            slidesToScroll: 1,
            dots: false,
            infinite: true,
            speed: 300,
            autoplay: true,
            arrows: true,
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(-1254 -507)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(1284 537) rotate(180)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
            responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
            }
          ]
        });

        $(elementClass).on( 'beforeChange', function( event, slick, currentSlide, nextSlide ) {
            if ( nextSlide == 0 ) {
                var cls = 'slick-current slick-active slick-center';

                // console.log('used');

                setTimeout(function() {
                    $( '[data-slick-index="' + slick.$slides.length + '"]' ).addClass( cls ).siblings().removeClass( cls );
                    for (var i = slick.options.slidesToShow - 1; i >= 0; i--) {
                        $( '[data-slick-index="' + i + '"]' ).addClass( cls );
                    }
                }, 10 );
            }
        });
    }
}

function partnersSlider() {
    if ($('body').hasClass('home') || $('body').hasClass('page-id-22')|| $('body').hasClass('page-id-461')|| $('body').hasClass('page-id-279')) {
        $('.partners-wrapper').slick({
            // centerMode: true,
            slidesToShow: 3,
            centerPadding: '2rem',
            slidesToScroll: 1,
            dots: false,
            infinite: true,
            speed: 300,
            autoplay: true,
            arrows: true,
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(-1254 -507)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g id="Forward_Arrow" data-name="Forward Arrow" transform="translate(1284 537) rotate(180)"><path id="Path_10" data-name="Path 10" class="cls-1" d="M15,0,12.273,2.727,22.6,13.052H0v3.9H22.6L12.273,27.273,15,30,30,15Z" transform="translate(1254 507)"/></g></svg>',
            responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
            }
          ]
        });
    }
}



function mobileButton() {
    $('.mobile-button').click(function(){
        $(this).toggleClass('active');
        $('#code_block-44-14').toggleClass('active');
    });
}

function history() {
  if ($(window).width() < 769) {
        $('.our-history-wrapper .our-history-item').first().addClass('active');
    }

  $('.our-history-wrapper .our-history-item > h4').click(function(){
       $('.our-history-item').each(function() {
        $(this).removeClass('active');
       });
      $(this).parent().addClass('active');
    });
}

function popup() {
    /* POPUP */
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = $(this).attr('data-popup-open');
        console.log('click');
        console.log(targeted_popup_class);

        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
        $('body').css('height', '100vh').css('overflow', 'hidden');
        $('div:not(.popup)').css('position', 'relative');
        
        if ($(window).width() < 1025) {
            $('.header').hide();
        }
            $('#section-50-16').hide();
            $('#section-6-22').hide();
            $('.footer').hide();

        e.preventDefault();

    });
 
    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = $(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        $('body').css('height', 'auto').css('overflow', 'visible');
       
       $('#section-50-16').show();
       $('#section-6-22').show();
       $('.footer').show();

        if ($(window).width() < 1025) {
            $('.header').show();
        }

        e.preventDefault();
    });
}

/*********************
HELPER METHODS
*********************/


