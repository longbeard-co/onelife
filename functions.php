<?php

// enqueue the child theme stylesheet

function wp_lb_enqueue_scripts() {

   wp_enqueue_style('childstyle', get_stylesheet_directory_uri() . '../../../plugins/nitrogen/assets/style.css', '2.2.3', true);
// wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '../../../plugins/nitrogen/assets/style.css'  );
// wp_enqueue_style( 'childstyle' );
wp_enqueue_script( 'lb_custom-js', get_stylesheet_directory_uri() . '../../../plugins/nitrogen/assets/js/lb_custom-v1.04.js', array( 'jquery' ), '1.04', true );
wp_enqueue_script( 'lity', get_stylesheet_directory_uri() . '../../../plugins/nitrogen/assets/js/vendors/lity.min.js', array( 'jquery' ), true );
wp_enqueue_script( 'flexibility', get_stylesheet_directory_uri() . '../../../plugins/nitrogen/assets/js/vendors/flexibility-master/flexibility.js', array( 'jquery' ), true );
};
add_action( 'wp_enqueue_scripts', 'wp_lb_enqueue_scripts', 11);

// Enqueue Google Analytics to header

function init_analytics() {
    $analyticsUA = 'UA-126960183-1';
    $analytics = '<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67479109-23"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag(\'js\', new Date());

  gtag(\'config\', \'' . $analyticsUA . '\');
</script>';
    
echo "\n" . $analytics;
}

add_action('wp_head', 'init_analytics', 35);

function init_pixel() {
    $pixelID = '123456789';
    $pixel = '<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,\'script\',
  \'https://connect.facebook.net/en_US/fbevents.js\');
  fbq(\'init\', \'' . $pixelID . '\');
  fbq(\'track\', \'PageView\');
</script>
<noscript><img height=\'1\' width=\'1\' style=\'display:none\'
  src=\'https://www.facebook.com/tr?id=' . $pixelID . ' &ev=PageView&noscript=1\'
/></noscript><!-- End Facebook Pixel Code -->';

echo "\n" . $pixel;
}


// add_action('wp_head', 'init_pixel', 35);

// Get custom post types
require_once('lb-cpt.php');


// filter the Gravity Forms button type
add_filter( 'gform_submit_button_1', 'subscribe_button', 10, 2 );
function subscribe_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Subscribe</span></button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_2', 'register_button', 10, 2 );
function register_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}


// add_filter( 'gform_submit_button_6', 'register_ind_button', 10, 2 );
function register_ind_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}


// add_filter( 'gform_submit_button_7', 'register_landing_button', 10, 2 );
function register_landing_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}


// filter the Gravity Forms button type
add_filter( 'gform_submit_button_3', 'contact_button', 10, 2 );
function contact_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_4', 'get_involved_button', 10, 2 );
function get_involved_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_5', 'register_full_button', 10, 2 );
function register_full_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}


// filter the Gravity Forms button type
add_filter( 'gform_submit_button_17', 'participa_full_button', 10, 2 );
function participa_full_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Entregar formulario</span></button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_23', 'order_full_button', 10, 2 );
function order_full_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}


// filter the Gravity Forms button type
add_filter( 'gform_submit_button_24', 'order_es_full_button', 10, 2 );
function order_es_full_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Enviar</span></button>";
}

// Disable scroll on certain form submissions

add_filter( 'gform_confirmation_anchor_4', '__return_false' );
add_filter( 'gform_confirmation_anchor_1', '__return_false' );
add_filter( 'gform_confirmation_anchor_16', '__return_false' );
add_filter( 'gform_confirmation_anchor_17', '__return_false' );


// add ACF options page
if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Images',
    'menu_title'  => 'OLLA Content',
    'menu_slug'   => 'olla-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'News Articles',
    'menu_title'  => 'News Articles',
    'parent_slug' => 'olla-content',
  ));
  
  
}


// Gravity Forms - Dynamic List

add_filter( 'gform_pre_render_28', 'populate_posts' );
add_filter( 'gform_pre_validation_28', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_28', 'populate_posts' );
add_filter( 'gform_admin_pre_render_28', 'populate_posts' );

add_filter( 'gform_pre_render_29', 'populate_posts' );
add_filter( 'gform_pre_validation_29', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_29', 'populate_posts' );
add_filter( 'gform_admin_pre_render_29', 'populate_posts' );

function populate_posts( $form ) {
 
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-groups-en' ) === false ) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $posts = get_posts( 'numberposts=-1&post_status=publish' );
        $groups = array();


        $entries = GFAPI::get_entries(array(28,34),$search_criteria['status'] = 'active', $sorting = null, $paging = array( 'offset' => 0, 'page_size' => 1000 ), $total_count = null);
        
 

         foreach($entries as $entry) {
            $group = $entry[12];
            $groups[] = array( 'text' => $group, 'value' => $group );
          }
        
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Group';
        $field->choices = $groups;
 
    }
 
    return $form;
}

// Gravity Forms - Dynamic List

add_filter( 'gform_pre_render_34', 'populate_posts_es' );
add_filter( 'gform_pre_validation_34', 'populate_posts_es' );
add_filter( 'gform_pre_submission_filter_34', 'populate_posts_es' );
add_filter( 'gform_admin_pre_render_34', 'populate_posts_es' );

add_filter( 'gform_pre_render_30', 'populate_posts_es' );
add_filter( 'gform_pre_validation_30', 'populate_posts_es' );
add_filter( 'gform_pre_submission_filter_30', 'populate_posts_es' );
add_filter( 'gform_admin_pre_render_30', 'populate_posts_es' );


function populate_posts_es( $form ) {
 
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-groups-es' ) === false ) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $posts = get_posts( 'numberposts=-1&post_status=publish' );
        $groups = array();


        $entries = GFAPI::get_entries(array(28,34),$search_criteria['status'] = 'active', $sorting = null, $paging = array( 'offset' => 0, 'page_size' => 1000 ), $total_count = null);
        
 

         foreach($entries as $entry) {
            $group = $entry[12];
            $groups[] = array( 'text' => $group, 'value' => $group );
          }
        
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Nombre del Grupo';
        $field->choices = $groups;
 
    }
 
    return $form;
}

if ( ! isset( $content_width ) ) {
    $content_width = 1400;
}