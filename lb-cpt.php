<?php 

// Register Custom Post Type
function speaker() {

    $labels = array(
        'name'                  => _x( 'Speakers', 'Post Type General Name', 'onelifeLA' ),
        'singular_name'         => _x( 'Speaker', 'Post Type Singular Name', 'onelifeLA' ),
        'menu_name'             => __( 'Speakers', 'onelifeLA' ),
        'name_admin_bar'        => __( 'Speakers', 'onelifeLA' ),
        'archives'              => __( 'Speakers Archives', 'onelifeLA' ),
        'attributes'            => __( 'Speakers Attributes', 'onelifeLA' ),
        'parent_item_colon'     => __( 'Parent Speakers:', 'onelifeLA' ),
        'all_items'             => __( 'All Speakers', 'onelifeLA' ),
        'add_new_item'          => __( 'Add New Speakers', 'onelifeLA' ),
        'add_new'               => __( 'Add New', 'onelifeLA' ),
        'new_item'              => __( 'New Speakers', 'onelifeLA' ),
        'edit_item'             => __( 'Edit Speakers', 'onelifeLA' ),
        'update_item'           => __( 'Update Speakers', 'onelifeLA' ),
        'view_item'             => __( 'View Speakers', 'onelifeLA' ),
        'view_items'            => __( 'View Speakers', 'onelifeLA' ),
        'search_items'          => __( 'Search Speakers', 'onelifeLA' ),
        'not_found'             => __( 'Not found', 'onelifeLA' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'onelifeLA' ),
        'featured_image'        => __( 'Featured Image', 'onelifeLA' ),
        'set_featured_image'    => __( 'Set featured image', 'onelifeLA' ),
        'remove_featured_image' => __( 'Remove featured image', 'onelifeLA' ),
        'use_featured_image'    => __( 'Use as featured image', 'onelifeLA' ),
        'insert_into_item'      => __( 'Insert into Speakers', 'onelifeLA' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Speakers', 'onelifeLA' ),
        'items_list'            => __( 'Speakers list', 'onelifeLA' ),
        'items_list_navigation' => __( 'Speakers list navigation', 'onelifeLA' ),
        'filter_items_list'     => __( 'Filter Speakers list', 'onelifeLA' ),
    );
    $args = array(
        'label'                 => __( 'Speakers', 'onelifeLA' ),
        'description'           => __( 'Speakers part of One Life LA' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'region' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 1,
        'menu_icon'             => 'dashicons-admin-users',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'speaker', $args );

}
add_action( 'init', 'speaker', 0 );


// Register Custom Post Type
function performer() {

    $labels = array(
        'name'                  => _x( 'Performers', 'Post Type General Name', 'onelifeLA' ),
        'singular_name'         => _x( 'Performer', 'Post Type Singular Name', 'onelifeLA' ),
        'menu_name'             => __( 'Performers', 'onelifeLA' ),
        'name_admin_bar'        => __( 'Performers', 'onelifeLA' ),
        'archives'              => __( 'Performers Archives', 'onelifeLA' ),
        'attributes'            => __( 'Performers Attributes', 'onelifeLA' ),
        'parent_item_colon'     => __( 'Parent Performers:', 'onelifeLA' ),
        'all_items'             => __( 'All Performers', 'onelifeLA' ),
        'add_new_item'          => __( 'Add New Performers', 'onelifeLA' ),
        'add_new'               => __( 'Add New', 'onelifeLA' ),
        'new_item'              => __( 'New Performers', 'onelifeLA' ),
        'edit_item'             => __( 'Edit Performers', 'onelifeLA' ),
        'update_item'           => __( 'Update Performers', 'onelifeLA' ),
        'view_item'             => __( 'View Performers', 'onelifeLA' ),
        'view_items'            => __( 'View Performers', 'onelifeLA' ),
        'search_items'          => __( 'Search Performers', 'onelifeLA' ),
        'not_found'             => __( 'Not found', 'onelifeLA' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'onelifeLA' ),
        'featured_image'        => __( 'Featured Image', 'onelifeLA' ),
        'set_featured_image'    => __( 'Set featured image', 'onelifeLA' ),
        'remove_featured_image' => __( 'Remove featured image', 'onelifeLA' ),
        'use_featured_image'    => __( 'Use as featured image', 'onelifeLA' ),
        'insert_into_item'      => __( 'Insert into Performers', 'onelifeLA' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Performers', 'onelifeLA' ),
        'items_list'            => __( 'Performers list', 'onelifeLA' ),
        'items_list_navigation' => __( 'Performers list navigation', 'onelifeLA' ),
        'filter_items_list'     => __( 'Filter Performers list', 'onelifeLA' ),
    );
    $args = array(
        'label'                 => __( 'Performers', 'onelifeLA' ),
        'description'           => __( 'Performers part of One Life LA' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'region' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 1,
        'menu_icon'             => 'dashicons-megaphone',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'performer', $args );

}
add_action( 'init', 'performer', 0 );

// Register Custom Post Type
function partners() {

    $labels = array(
        'name'                  => _x( 'Partners', 'Post Type General Name', 'onelifeLA' ),
        'singular_name'         => _x( 'Partner', 'Post Type Singular Name', 'onelifeLA' ),
        'menu_name'             => __( 'Partners', 'onelifeLA' ),
        'name_admin_bar'        => __( 'Partners', 'onelifeLA' ),
        'archives'              => __( 'Partners Archives', 'onelifeLA' ),
        'attributes'            => __( 'Partners Attributes', 'onelifeLA' ),
        'parent_item_colon'     => __( 'Parent Partners:', 'onelifeLA' ),
        'all_items'             => __( 'All Partners', 'onelifeLA' ),
        'add_new_item'          => __( 'Add New Partners', 'onelifeLA' ),
        'add_new'               => __( 'Add New', 'onelifeLA' ),
        'new_item'              => __( 'New Partners', 'onelifeLA' ),
        'edit_item'             => __( 'Edit Partners', 'onelifeLA' ),
        'update_item'           => __( 'Update Partners', 'onelifeLA' ),
        'view_item'             => __( 'View Partners', 'onelifeLA' ),
        'view_items'            => __( 'View Partners', 'onelifeLA' ),
        'search_items'          => __( 'Search Partners', 'onelifeLA' ),
        'not_found'             => __( 'Not found', 'onelifeLA' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'onelifeLA' ),
        'featured_image'        => __( 'Featured Image', 'onelifeLA' ),
        'set_featured_image'    => __( 'Set featured image', 'onelifeLA' ),
        'remove_featured_image' => __( 'Remove featured image', 'onelifeLA' ),
        'use_featured_image'    => __( 'Use as featured image', 'onelifeLA' ),
        'insert_into_item'      => __( 'Insert into Partners', 'onelifeLA' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Partners', 'onelifeLA' ),
        'items_list'            => __( 'Partners list', 'onelifeLA' ),
        'items_list_navigation' => __( 'Partners list navigation', 'onelifeLA' ),
        'filter_items_list'     => __( 'Filter Partners list', 'onelifeLA' ),
    );
    $args = array(
        'label'                 => __( 'Partners', 'onelifeLA' ),
        'description'           => __( 'Partners part of One Life LA' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'region' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 1,
        'menu_icon'             => 'dashicons-admin-multisite',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'partners', $args );

}
add_action( 'init', 'partners', 0 );

 ?>